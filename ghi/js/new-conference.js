window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('location')
      for(let location of data.locations){
        const optionElement = document.createElement('option')
        //const id = location.href
        optionElement.value = location.id //id.charAt(id.length - 2)
        optionElement.innerHTML = location.name
        selectTag.appendChild(optionElement)
      }
      const formTag = document.getElementById('create-conference-form')

      formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
              'Content-Type': 'application/json',
            },
          }
          const response1 = await fetch(conferenceUrl, fetchConfig)
          if (response1.ok) {
            formTag.reset()
            const newConference = await response1.json()
            console.log(newConference)
          }
      })
    }

  });
