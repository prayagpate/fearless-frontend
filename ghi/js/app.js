
function createCard(name, description, pictureUrl, starts, ends, locationName) {
    const formatStart = new Date(starts).toLocaleDateString()
    const formatEnd = new Date(ends).toLocaleDateString()
    return `
      <div class="card shadow mb-4 shadow-lg">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${formatStart} - ${formatEnd}
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/'
    try{
        const response = await fetch(url)
        if(!response.ok){
            console.log("Response not OK!")

        }else {
            const data = await response.json()

            // const conference = data.conferences[0]
            // const nameTag = document.querySelector('.card-title')
            // nameTag.innerHTML = conference.name
            //console.log(conference)
            for(let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok) {
                    const details = await detailResponse.json()
                    const title = details.conference.name
                    const description = details.conference.description
                    const pictureUrl = details.conference.location.picture_url
                    const starts = details.conference.starts
                    const ends = details.conference.ends
                    const locationName = details.conference.location.name
                    //const detailsTag = document.querySelector('.card-text')
                    //detailsTag.innerHTML = description
                    //const pictureTag = document.querySelector(".card-img-top")
                    //pictureTag.src = details.conference.location.picture_url
                    const html = createCard(title, description, pictureUrl, starts, ends, locationName)
                    const column = document.querySelector('.col')
                    column.innerHTML += html

                }
            }
        }
    }  catch (e){
        //console.error(e)
    }

});
