import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import AttendeeForm from "./AttendeeForm";

import LocationForm from './LocationForm';
import LocationList from './LocationList';

import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from "./AttendConferenceForm";

import MainPage from "./MainPage";
import PresentationForm from "./PresentationForm";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route index element={<MainPage />} />
          <Route path="attendees" element={<AttendeesList />} />
          <Route path="attendees/new" element={<AttendeeForm />} />

          <Route path="/locations" element={<LocationList />} />
          <Route path="/locations/new" element={<LocationForm />} />

          <Route path="conferences/new" element={<ConferenceForm />} />

          <Route path="presentations/new" element={<PresentationForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
